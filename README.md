# [Info](#info)

I use two browsers to analyze and show screenshots in this article. Brave Browser blocks ads by default, so I used Falkon Browser to create the white screenshots and in order to compare it against Brave to get an more objective viewpoint. Falkon comes also with an integrated adblocker but it is easier to create screenshots when both browsers are opened to cross-compare what is going on, disabling the ad-blocker in Falkon is done with only three clicks.


[Deskmodder.de](https://www.deskmodder.de/blog/) is a German Blog, mainly written by [moinmoin](https://www.deskmodder.de/blog/author/moinmoin/) who seems to be jobless. The story reminds me on [TheHackernews](https://attrition.org/errata/plagiarism/thehackernews/). Germany btw has pretty strong [copyright laws](https://www.gesetze-im-internet.de/englisch_urhg/englisch_urhg.html).


In a nutshell
- moinmoin is someone who copies other people content from RSS-Feeds, Twitter, [My Digital Life Forums](https://forums.mydigitallife.net/) in short MDL Forum and then translate it into German, in order to beg for money and attention to lure people on the Website.
- The intention why he does it to get users in his shady website to make money, this is obvious because the website s full of trackers, ad-banners and banners trying to convince you to donate for their work.
- Since I am also involved in tech I can directly say that Deskmodder.de plagiarize others because I see the real posted content and then the German translated Deskmodder.de version of it. They seems to translate Twitter tweets and represent it as their own content.
- [Deskmodder.de is hosted by Hetzner Online GmbH](https://dnslytics.com/domain/deskmodder.de) and seems to use as CMS Wordpress.


## [Ads, tracking, ref links and begging for money for content Deskmodder simply translates from English into German](#ads-tracking-ref-links-and-begging-for-money-for-content-deskmodder-simply-translates-from-english-into-german)

![The website is full of ads if you do not use an ad-blocker](https://i.ibb.co/ZNKPXq4/deskmodder-ads.png)

Ads between articles, this also happens in their forum between posts.


![Trackers](https://i.ibb.co/0MgFCb5/deskmodder-trackers.png)

Trackers integrated to make cash and for user statistics.


![Trackers](https://i.ibb.co/qD4q28m/no-source.png)

No source, just quickly translated from the Microsoft website without linking the original KB article. Moinmoin uses DeepL to translate english article into german, that is all.


![Trackers](https://i.ibb.co/BLHp2jJ/no-image-credit.png)


No image credit, which he simply took from [here](https://www.ventoy.net/en/doc_non_destructive.html), cut into half for no reason and uploaded it. Instead of just linking the announce article regarding the new change he could have just embed the [original Image](https://www.ventoy.net/static/img/shrink_en.png) and add the link into his news, but he had to cut it and upload it which makes absolute no sense at all.


![Ref links](https://i.ibb.co/rZP1XBP/deskmodder-ref-links.png)


![Amazon begging](https://i.ibb.co/0XpHGDH/begging.png)


Ref links and begging.


![Ref links](https://i.ibb.co/PDS01hT/deskmodder-forum-ads.png)


Forum ads like it is 1999 to make even more cash. Thanks to aunt Google.


Talking about money, they collect some donations but never disclose how many they collect nor have any, thank you, page. No monthly or yearly statistics, just nothing.


![Translated into German and then stolen](https://i.ibb.co/qdgMJ1G/dsd.png)


Once again stolen and re-written simply into German, this time [moinmoin added a random name](https://www.deskmodder.de/blog/2021/11/06/android-apps-als-apk-unter-windows-11-schon-jetzt-installieren-auch-in-deutsch//) instead of the real link which is `https://www.htnovo.net/2021/10/come-installare-app-android-windows-11-apk.html`. You even see that this was updated in a hurry, symbol after Update `]` you would think that someone who writes articles daily checks his articles and not randomly copy templates into existing articles and then even forgot to remove parts of it. Not only unprofessional but shows that he adds things in a hurry. Btw I do not say I am perfect, I also suck regarding writing but I admit that and whenever I add things I give proper credit with links and background information. I think everyone edited his articles because you found afterwards some stuff, it is not about that, it is about that he tries to be the first in order to rank higher in search results, fooling and pretending he discovered it.


![Translated into German and then stolen](https://i.ibb.co/10mgKTz/dddddd.png)

Even if you translate something you need to give full credit, the name and the link, which not happened in this case, as you can see on the same thread at the bottom, absolute no source link mentioned.


![Stolen feedback Hub article](https://i.ibb.co/GkF7Xmv/feedback.png)

Idea stolen from [here](https://answers.microsoft.com/en-us/windows/forum/all/the-feedback-hub-doesnt-let-i-post-any-feedback/4545c939-7b39-4f0f-b06c-7cb4a4b2537d) without any credit, just recreated with the German Feedback Hub app. It could have be legitimate news with credit alias mentioning the source and without watermark in the image. Deskmodder has no copyright on the idea nor the Feedback Hub App itself, they did not created anything here. They do this watermark thing because other people or pages do the same, this is done to promote their website.



![Stolen from MDL Forum without any Credit](https://i.ibb.co/gWtwmqq/Alt-Tab-Ansicht.png)


![Stolen from MDL](https://i.ibb.co/xHmbZt5/alt-tabstolen.png)


Once again busted [stealing from MDL](https://forums.mydigitallife.net/threads/discussion-windows-10-insider-preview-build-20161-1000-pc-fast-dev-channel-rs_prerelease.81990/page-4#post-1606164) without giving any Credit. The only thing what Deskmodder changed is the random generated folder ID, which they replaced to make it work again in latest Windows Insider Builds. The original finding or credit for this original finding was never given, fooling people that Deskmodder found this on their own, which was not the case. I also doubt they found the ID on their own, it is more likely that someone on Twitter posted the new ID and they took it, hard to tell.


![No source or image credit](https://i.ibb.co/MkgkS3k/no-credit.png)

No source or image credit given. Title similar like article form [Anandtech](https://www.anandtech.com/show/17070/mediatek-announces-dimensity-9000) and pictures possible taken from [here](https://liliputing.com/2021/11/mediatek-dimensity-9000-is-a-true-flagship-class-processor.html).


## [Confronting them results in a ban or your post does not get approved](#confronting-them-results-in-a-ban-or-your-post-does-not-get-approved)

![Criticism not approved](https://i.ibb.co/PZzRjNX/1.png)


Their website contains a comment section as well as a Wiki and Forum. You can comment on their articles directly even without registering yourself an account on their website, however the have an IP check and pretty much a spam plugin installed which reviews your comments, like most Wordpress blogs these days.


I confronted the Deskmodder Team in their [Telegram channel](https://t.me/deskmodder) asked them why they never give credit on their Website for content the simply translate via [DeepL](https://www.deepl.com/en/translator) on their website and after some discussions you get banned for no reasons. MoinMoin only said something that I was annoying, well the truth can be annoying. I kept no log because that is not how I am, I normally try to make things right and if I have a problem I say it loud and clear without logging things, but maybe I should consider this in the future.


I even tried to post there and registered an account but after more and more people confronted me with the fact that Deskmodder steals content I deleted my account there.


## [Deskmodder loves MDL but got banned from MDL due to content theft](#deskmodder-loves-mdl-but-got-banned-from-mdl-due-to-content-theft)

[MDL](https://forums.mydigitallife.net/) is a well-known forum in the Windows community for all sorts of tricks, tips, mods for Windows and of course Deskmodder is there too, monitoring everything.

Yen, one of the MDL Admins among other Mods and Admins noticed Deskmodder, their website and checked their content, and it points out they gave no credit while hosting and mirroring stuff exclusive posted in MDL Forums. You actually need to be a registered member to see links on MDL, this is made on purpose, not only to avoid that Google can index the links, also to avoid people stealing forum members work easily via google search result entries.

Deskmodder, even the word got banned. The URL as well as the word Deskmodder.de is entirely blocked because of this on MDL forums. This is a result for their actions.


## [Moinmoin loves to edit posts and content afterwards](#moinmoin-loves-to-edit-posts-and-content-afterwards)

MoinMoin seems to have absolute no hobbies, he edits and manipulates how he pleases. To be fair here Deskmodder has some guest articles, those entries seems to be legitimate written by people who just want to share their opinions, thoughts and news.


![Copies](https://i.ibb.co/yqn2WYt/2.png)


As you can see Deskmodder alias MoinMoin mirrored once again a MDL script without any permission, remember you need to be a registered member in MDL forum to see links. So he did login, downloaded the script and re-uploaded it without even bothering asking the original author if that is okay or wanted. I mean how could he if even the word deskmodder.de is already blocked, so clearly the MDL Forum staff does not want to have anything to do with Deskmodder.

If you monitor their website with web-crawler, archive and some other tools I am not going to mention here, you see that MoinMoin very often copies content, even in unfinished form, translates it later. This is a practice for SEO for Search Indexers to be the first and to rank possible because of the so-called new content higher.


What he does, is to monitor some websites, translate it to German and basically begs for money.


## [Editing content when you get caught to escape DMCA takedown requests](#editing-content-when-you-get-caught-to-escape-dmca-takedown-requests)

I monitor Deskmodder since their early beginning because they also stole content from me without asking, the weird thing is that at the beginning I would have given my permission if they name me as author and original source but that never happened.

Time went by and Deskmodder got bigger, because of what they do best, translating stuff into German, content taken from others. As of today, sometimes the link the original source and sometimes not and no one knows why. Maybe it is moinmoins strategy to never add a source, pretend he is the original author and then when he get caught he quickly edits it and add a source.

![No versions history](https://i.ibb.co/k0cDM9r/deskmodder-date.png)

Deskmodder.de has no versions history or version control, or a last edited by on their Articles nor their Wiki. So this method seems to work well since no one can prove, easily, when and what exactly was the last time edited. The only way to capture versions is to work with the Wayback Machine, however WB only allows snapshots taken every 45 min, so you need to use other tools which I am for several reason not mention here, manly to give MoinMoin no chance to do counter measures.

![Sources can be added and edited at any time](https://i.ibb.co/P5Wc19K/deskmodder-sources.png)


Missing author infos and description makes it hard to verify, or impossible like in this case to quickly check if the author is credible or not, because you cannot research these ghosts.

Those fields can be edited and manipulated afterwards.


## [First Report from me](#first-report-from-me)

You can report them to their hoster, Hetzner via [abuse form](https://abuse.hetzner.com/issues/new?lang=en). I did this after years because at some point it is enough, especially if money is involved. I reported them for their stolen TPM Tweak which they list in their mostly copied and translated [Wiki](https://www.deskmodder.de/wiki/).

They basically took my posted TMP tweak and pretended they found it by not mention the original author. The link to this is [here](https://www.deskmodder.de/wiki/index.php?title=Windows_11_auch_ohne_TPM_und_Secure_Boot_installieren).

![Stolen TPM Tweak](https://i.ibb.co/zPXdgrb/Deskmodder-stolen-tpm.png)

As you can see above, Deskmodder steals other people tweaks, in this case my own and does not give any credit, there is no source mentioned. I posted the tweak on Twitter, Reddit and GitHub. After I reported this because I found that it is now enough they quickly added some random source on GitHub. Too bad it was my own alt account on GitHub, I reported it once again and now no source is mentioned. I wonder how many times he edited it back and forwards and how many revisions the document must have. Hard to tell without any revisions history.

This is shady behavior and spits everyone in the face, especially the original author. Their entire Wiki and Blog is full of such things, things they never invented but refuse to give credit or link the original source, which is mostly MDL, some Twitter people including myself.


## [More translated content labeled as their own](#more-translated-content-labeled-as-their-own)

![Stolen Tweak - No credit given](https://i.ibb.co/1qv2mSb/2022-02-22-14-43.png)

- [Deskmodder Version](https://www.deskmodder.de/blog/2022/02/22/windows-11-systemanforderungen-werden-nicht-erfuellt-in-den-einstellungen-und-wasserzeichen-einfach-entfernen/)
- [Original taken from here without any Credit or Link to the original thread](https://old.reddit.com/r/Windows11/comments/sw2ueq/main_settings_and_watermark_now_warns_you_that_it/)


## [Made up story about Windows 12 internal Sources](#made-up-story-about-windows-12-internal-sources)

Even after [Deskmodder got busted by their own community that the Windows 12 story is a joke](https://www.deskmodder.de/blog/2022/02/20/windows-12-microsoft-beginnt-ab-maerz-mit-der-arbeit/#comments), they still claim they have internal sources to back a joke from SwiftonSecurity up. One day later the story is still up on their website pretending they have something to release, which is nothing but a lie. Instead of admitting they made a mistake and remove the false story or update it they let it stand. The backup [archive is here](https://web.archive.org/web/20220221194105/http://web.archive.org/screenshot/https://www.deskmodder.de/blog/2022/02/20/windows-12-microsoft-beginnt-ab-maerz-mit-der-arbeit/).


## [More Content Theft](#more-content-theft)

Deskmodder seems to never stop, this time they stole from Armin Osaj.

![No credit and no mention of the source](https://i.ibb.co/bPgKRxV/Explorer-Tabs-no-credit.png)

![Time shows the original post was uploaded first by Armin Osaj](https://i.ibb.co/8D4zsd5/Explorer-Tabs-Time.png)


[Deskmodders article](https://www.deskmodder.de/blog/2022/03/10/windows-11-tabs-im-datei-explorer-sind-zurueck-gleich-mal-getestet/) and then the [original article by Armin Osaj](https://windowsarea.de/2022/03/datei-explorer-bekommt-tabs-jetzt-aktivierbar-fuer-es-insider/) that was 9 hours earlier posted and uploaded on the Internet.


He just took `vivetool.exe addconfig 34370472 2` and re-wrote it without mention were he took it from. No credit just nothing, pretending Moinmoin found it, which was not the case.


## [Payback after I reported them to Hetzner](#payback-after-i-reported-them-to-hetzner)

After I reported them and they got forced to edit their Wiki, they reported my WordPress Blog, the Blog that is linked above alias[chefkochblog.wordpress.com](https://chefkochblog.wordpress.com/). I have of course, made some backups and nothing is done, most part is archived and I contacted today WordPress that this is a bogus and made-up abuse report, because I got not even a reason shown why my should be taken offline after years. There was no eMail and nothing I could respond too. However my dashboard showed me that I can contact WordPress and as said, I did that today. I did not even noticed that my Blog was offline because my brother told me and once again, I got no eMail.

This is a little bit unprofessional from Wordpress because you should get some time to respond and react to an abuse report. There was again absolute no eMail and again no Reason specified why my Blog is taken offline right-now and of course no one has the backbone to reveal his face. I only know it was Deskmodder because it happened right after I reported them over to Hetzner.


This is Kindergarten on the highest level because I backup my stuff anyway and if you wrongfully report someone then this will end up that WordPress will just ignore those troll reports, which only would help me because that would make me immune. My blog is now years running without any issue, no reports nothing, this was clearly a payback action that I dared to report them.


**Update 03.11.2021**

My [WordPress Blog is back online](https://twitter.com/CKsTechNews/status/1455818087807307778), WordPress was pretty quick here.


## [Info regarding copyright trolling](#info-regarding-copyright-trolling)

You do not need to bother sending [bogus DMCA requests](https://www.hinchnewman.com/internet-law-blog/2015/04/legal-copyright-dmca-takedown-notice/) against me now, I know Deskmodder.de Team and others will read my little article here.

I did backups of everything and created archive mirrors to all of this. If you take this down with made-up arguments, I will host this on my own server, rest assured. I know the trolls will come and try removing this guide from the internet, I am prepared.


## [Conclusion](#conclusion)

Deskmodder.de or shall I say moinmoin, because he steals the posts from others, is a shady website that uses a [parasitism](https://en.wikipedia.org/wiki/Parasitism) mechanism to feed off others to make money. Deskmoder has so much today with modding than GHacks with Google Hacks these days, absolute nothing. What they do is content scraping and translating it into German, nothing more, on top of that they plagiarize others pretending they are the original authors by not giving proper attribution where credit's due.

Reporting them is almost pointless because they edit their stuff afterwards anyway, pretending it was always there because they have no website revisions history which makes it easy for them to forge what was really posted to which time, the only things that are shown are when the post was created but not the last edit time, who edited it and what exactly got changed. Let me say that I also edit my post same like other websites but we normally indicate updates with tags or changed timestamps so that people are aware what was edited and when new things got added. We are not talking about small things like typos, we are talking about essential stuff that changes the original article, that also includes giving credit, mention the original source and label pictures correctly.

Deskmodder.de are nothing but thieves, that often promote closed source software mainly to make a deal out of it. Their website changed over the years but never their method how they steal others people content. You can pretty easily bust them with some web-crawlers by monitoring their website and their articles to reveal that they often just copy and paste others.

They have no own sources and are not reliable regarding truth in general because they never disclose all their sources, which would be expected from so-called modders and news writers.

It is a shame that Hetzner does not take action here as they should and remove the website/domain for good, because you get the same content from the true sources and that is mostly MDL, Twitter etc. I think every German these days can speak or at least read english because that is what you learn in every german school these days.

--------------------------------------------------

## [Bonus - Computerbase wannabe journalist copies the ones who copies others](#bonus---computerbase-wannabe-journalist-copies-the-ones-who-copies-others)

[Sven Bauduin](https://www.computerbase.de/autor/sven.bauduin/3/) seems to be a big fan of Deskmodder.de, because whenever Deskmodder publish an article, Mr. Bauduin seems to rewrite it and release it on Computerbase.de. What a coincidence.

### Example 1
- [https://www.deskmodder.de/blog/2021/11/03/amazon-fire-tv-stick-4k-max-neuer-streaming-stick-weitere-amazon-geraete-aktuell-reduziert/](https://www.deskmodder.de/blog/2021/11/03/amazon-fire-tv-stick-4k-max-neuer-streaming-stick-weitere-amazon-geraete-aktuell-reduziert/)
- [https://www.computerbase.de/2021-11/fire-tv-fire-hd-und-echo-amazon-bietet-smart-home-und-tablets-reduziert-an/](https://www.computerbase.de/2021-11/fire-tv-fire-hd-und-echo-amazon-bietet-smart-home-und-tablets-reduziert-an/)

### Example 2
- [https://www.deskmodder.de/blog/2021/11/03/fritzbox-7590-und-7590-ax-bekommen-fritzos-7-29/](https://www.deskmodder.de/blog/2021/11/03/fritzbox-7590-und-7590-ax-bekommen-fritzos-7-29/)
- [https://www.computerbase.de/2021-11/avm-fritz-os-7.29-fritz-box-6591-cable-und-6660-cable-erhalten-update/](https://www.computerbase.de/2021-11/avm-fritz-os-7.29-fritz-box-6591-cable-und-6660-cable-erhalten-update/)

### Example 3
- [https://www.computerbase.de/2021-10/windows-11-sekundaere-anzeige-erhaelt-uhr-dank-evenclock/](https://www.computerbase.de/2021-10/windows-11-sekundaere-anzeige-erhaelt-uhr-dank-evenclock/)
- [https://www.deskmodder.de/blog/2021/10/23/elevenclock-uhrzeit-auch-auf-dem-zweiten-monitor-unter-windows-11-inkl-sekunden/](https://www.deskmodder.de/blog/2021/10/23/elevenclock-uhrzeit-auch-auf-dem-zweiten-monitor-unter-windows-11-inkl-sekunden/)


### Example 4
- It goes on and on, you can check [his profile](https://www.computerbase.de/autor/sven.bauduin/3/) and then cross-compare it with the [Deskmodder.de](https://www.deskmodder.de/) website.

--------------------------------------------------

If you check the article timestamps you see Computerbase.de publish their stuff later, exactly in the interval so that Mr. Bauduin has enough time to rewrite it and publish his version of it. That he sympathize with them is clear with this [article](https://www.computerbase.de/2021-11/drag-und-drop-in-der-taskleiste-microsoft-macht-eine-rolle-rueckwaerts-bei-windows-11/), he even calls Deskmodder out as source. My brother alias Rin actually said in CB forum that Deskmodder has no own sources and showed where they copied it, yet he refused to change his article which is nothing but wrong. Deskmodder copied the info once again from Windows Latest and MDL page without mention those real sources on their article.

He probably copies other websites too, hard to tell, and then rewrites it, this is not how journalism is supposed to work. It is disappointing to see that Computerbase.de allows such clowns to write on their front page. This is absolute no coincidence and cannot be used as excuse. You maybe similar with maybe one, maybe two articles but not always. Publishing articles at the same date and shorty after each other is not common for journalists, as if they have the same source even if Deskmodder randomly reports about an GitHub project which was nowhere mentioned. Even if you pull the excuse that you setup RSS feeds, it would not explain how random and small GitHub projects getting their own article, right after page x reports about them, this is only possible if you subscribe to that page x and rewrite it and call it news without mention where you got that idea or news in the first place.

This is a new low for Computerbase.de, no other author on their website does such thing.  In most cases they rely on RSS Feeds but mention their sources and do not rewrite already existing articles on other pages. Deskmodder is already shady but copy them or mention them as source for anything is nothing but cringe and has nothing to do with journalism. He seems to be biased and a fan of them, there is no other explanation who else would subscribe them and then rewrite their shady edited articles... No one...


## [More screenshots and evidence will be added in time](#more-screenshots-and-evidence-will-be-added-in-time)

To be continued and to be edited soon. 😉

